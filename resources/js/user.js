export default {
        
    data() {
        return {
        estado: '',
        options: [
            { value: '', text: 'Seleccione un estado' },
            { value: '1', text: 'Activo' },
            { value: '0', text: 'Desactivo' },         
        ]
        }
    },        
    mounted() {
        console.log('Component mounted.')
    }
}