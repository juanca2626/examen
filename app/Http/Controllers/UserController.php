<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {        
        $estado = $request->get('estado');
        if($estado != ''){
            $users = User::where('status', $estado)->orderBy('name');
        }else{
            $users = User::orderBy('name');
        }
 
        $users = $users->paginate(5);

        return response()->json($users);

    }

    public function updateStatus($id)
    {
        $user = User::find($id);

        if ($user->status == "1"){

            $user->status = "0";
            $user->save();
            return response()->json(["message"=>"Estado actualizado exitosamente"]);
        }else{

            $user->status = "1";
            $user->save();
            return response()->json(["message"=>"Estado actualizado exitosamente"]);
        }

    }

}
