
## Herramientas Básicas

El desarrollo esta basado en:

- [Laravel 7](https://laravel.com).
- [VueJS 2](https://vuejs.org).

## Inicio

Para iniciar el desarrollo se deben seguir los siguientes pasos:

1. Instalar [Composer](https://getcomposer.org)
2. Instalar [Git](https://git-scm.com)
3. Clonar el repositorio
4. Configurar el archivo .env
5. Ejecutar: `composer install`
6. Ejecutar: `npm install`
7. Ejecutar: `php artisan key:generate` 
8. Ejecutar `php artisan migrate --seed`
9. Ejecutar `npm run dev`

## Base de datos
El ejecutar el migrate con el seed se creara la tabla user con 20 mil registro.
modificado






 